from openpyxl import Workbook
from openpyxl import load_workbook

tamplate_path   = "/tmp/"
result_path     = "/tmp/"     

def return_ws(tamplate_name):
    wb = load_workbook(tamplate_path + tamplate_name)
    ws = wb.active
    return {'wb': wb, 'ws': ws}


# Wtite date to xlsx tamplate
def writeDataToTamplete(tamplate_name, result_name, data):

    xlsx = return_ws(tamplate_name)
    
    for el in data:
        xlsx['ws'].cell(el[0], el[1]).value = el[2]

    xlsx['wb'].save(result_path + result_name)



