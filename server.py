#/usr/bin/python3

import os
import excel as xl
import get_region as region_reader
import gevent
import signal
import excel_deserializer

from gevent.pywsgi import WSGIServer
from flask import (request, Flask)

app = Flask(__name__)

@app.route("/service-status", methods=['GET'])
def service_status():
    return "service-status: active"

@app.route("/api/v1/fill_tamplate", methods=['POST'])
def fill_tamplate():
    
    rq = request.json
        
    if (rq == None):
        return "Ошибка: не преданы параметры запроса к сервису"

    xl.writeDataToTamplete(rq['xlsx_tample_path'], rq['result_file_name'], rq['value_data'])
    return "OK"

@app.route("/api/v1/get_region", methods=['POST'])
def get_region():
    
    rq = request.json
        
    if (rq == None):
        return "request.json is empty"

    region = rq['region']

    result, err = region_reader.get_region_data(rq['xlsx_file'], rq['sheet_name'], region['top'], region['left'], region['bottom'], region['right'])
    
    if err['code'] != 0:
        return err

    result_string = ""

    for row in result:
        str_col = ""
        for col in row:
            str_col = str_col + str(col) + "$;$"
        str_col = str_col[:-3] 
        
        result_string = result_string + str_col + "$%$"
    result_string = result_string[:-3]

    return result_string
	
	
@app.route("/api/v1/save_excel_data", methods=['POST'])
def save_excel_data():
    if request.json is not None:
        wb = excel_deserializer.load_workbook(request.json)
        wb.save(filename=request.json["book_name"])
    return "Ok"	
    

def main():
    try:
        host = os.environ.get('excel_host', '0.0.0.0')
        port = int(os.environ.get('excel_port', 5125))
        print ('Starting up on %s:%d' % (host, port))
        http_server = WSGIServer((host, port), app)

        def stop():
            print ('Handling signal')
            if http_server.started:
                http_server.stop()

        gevent.signal(signal.SIGTERM, stop)
        http_server.serve_forever()

    except (KeyboardInterrupt, SystemExit):
        print ('Stopping')
        if http_server.started:
            http_server.stop()
    finally:
        print ('Stoped')

if __name__ == "__main__":
    main()
    # Debug/Development
    #app.run(host=excel_host, port=excel_port)       