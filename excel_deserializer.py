# -*- coding: utf-8 -*-

import json
import codecs
import os.path

from openpyxl import Workbook
from openpyxl.styles import (
    PatternFill,
    Font,
    Border,
    Side,
    Alignment,
    NamedStyle,)


def detect_encoding(data):
    head = data[:4]
    if head[:3] == codecs.BOM_UTF8:
        return 'utf-8-sig'
    return 'utf-8'


def deserialize_data(s):
    data = None
    s = None
    filepath = "d:\data.json"
    if os.path.exists(filepath):
        with open(filepath, "rb") as fp:
            s = fp.read()
            encoding = detect_encoding(s)
            s = s.decode(encoding)
        if s is not None:
            data = json.loads(s)
    return data


def get_styles(styles, style_description):
    for name, descr in style_description.items():
        if name in styles.keys():
            continue
        
        style = NamedStyle(name=name)

        top = Side(style=descr["btop"], color="000000")
        left = Side(style=descr["bleft"], color="000000")
        right = Side(style=descr["bright"], color="000000")
        bottom = Side(style=descr["bbottom"], color="000000")
        style.border = Border(top=top, left=left, right=right, bottom=bottom)

        if descr["fgColor"] is not None:
            style.fill = PatternFill("solid", fgColor=descr["fgColor"])

        style.alignment = Alignment(horizontal=descr["ahorizontal"], vertical=descr["avertical"], textRotation=0, wrapText=descr["awrap_text"], shrinkToFit=False)
        style.font = Font(name=descr["fname"], size=descr["fsize"], b=descr["fbold"], color=descr["fcolor"])

        styles[name] = style

    return styles


def fill_worksheets(wb, sheets):
    wb_styles = {}
    for sheet in sheets:

        ws = wb.create_sheet(sheet["title"])
        get_styles(wb_styles, sheet["styles"])
        sheet_data = sheet["data"]
        properties = sheet["properties"]

        row_count = len(sheet_data)
        for i in range(row_count):
            for col, v in sheet_data[i].items():
                row = i + 1
                cell_name = "{}{}".format(col, row)
                cell = ws[cell_name]

                try:
                    val = float(v["val"])
                except ValueError:
                    val = v["val"]

                cell.value = val
                style = wb_styles.get(v["style_id"])
                if style is not None:
                    cell.style = style

        for merge in sheet["merges"]:
            ws.merge_cells(merge)
        for g in sheet["groups"]:
            if g["type"] == "hg":
                ws.column_dimensions.group(g["start"], g["end"], g["level"], False)
            elif g["type"] == "vg":
                ws.row_dimensions.group(g["start"], g["end"], g["level"], False)

        if properties is not None:
            ws.sheet_properties.outlinePr.summaryRight = properties.get("summaryRight", True)


def load_workbook(data):
    wb = Workbook()
    for ws in wb:
        wb.remove(ws)
    fill_worksheets(wb, data["sheets"])
    return wb
