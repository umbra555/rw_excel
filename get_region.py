import os
from openpyxl import Workbook
from openpyxl import load_workbook

# env workdir
data_folder = os.environ.get('excel_data_path', './data/')


def return_ws(file_name, sheet_name):

    print("Открыть книгу: ", file_name)
    try:
        wb = load_workbook(data_folder + file_name,
                           read_only=True, data_only=True)
    except Exception:
        print("Файл: " + data_folder + file_name + " не найден!")
        return {}, {'code': 1, 'msg': "Файл: " + data_folder + file_name + " не найден!"}

    print("Выбрать лист: ", sheet_name)

    if sheet_name in wb.sheetnames:
        ws = wb[sheet_name]
    else:
        sheetindexs = {wb.index(sheet): sheet for sheet in wb.worksheets}
        ws = sheetindexs.get(0, None)
    
    return {'wb': wb, 'ws': ws}, {'code': 0}

# Export func
def get_region_data(file_name, sheet_name, up, left, bottom, right):
    
    err = {'code': 0}

    file, err = return_ws(file_name, sheet_name)
    
    if err['code'] == 1:
        return [], err['msg']
    
    if err['code'] == 2:
        file['wb'].close()
        return [], err['msg']

    ws = file['ws']
    
    print("Лист открыт")

    up      = int(up)
    left    = int(left)
    bottom  = int(bottom)
    right   = int(right)

    if bottom == 0:
        bottom = ws.max_row
    
    if right == 0:
        right = ws.max_column
    
    if bottom > ws.max_row:
        bottom = ws.max_row
    
    if right > ws.max_column:
        right = ws.max_column

    up      = max(up, 0)
    left    = max(left, 0)
    bottom  = max(bottom, 0)
    right   = max(right, 0)

    #rows_iter = ws.iter_rows(min_col=left, min_row=up, max_col=right, max_row=bottom)
    #data = [[cell.value for cell in row] for row in rows_iter]

    data = [[i.value for i in j] for j in ws.get_squared_range(left, up, right, bottom)]

    file['wb'].close()   

    return data, err
